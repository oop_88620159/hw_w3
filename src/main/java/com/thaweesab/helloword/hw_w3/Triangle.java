/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w3;

/**
 *
 * @author acer
 */
public class Triangle {
    double  h,b;
    public Triangle(double h,double b){
        this.h = h;
        this.b = b;
        
    }
    public double AreTri(){
        return 0.5*b*h ;
    }
    public void setTriH(double h){
        if(h<=0){
            System.out.println("Error: H must more than zero!!!");
            return;
        }
        this.h =h;
    }
     public void setTriB(double b){
        if(b<=0){
            System.out.println("Error: B must more than zero!!!");
            return;
        }
        this.b =b;
    }
    
}
