/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w3;

/**
 *
 * @author acer
 */
public class Square {
    double s ;
    public Square (double s){
        this.s = s;
    }
    public double AreSqu(){
        return s*s;
    }
    public void setSquS(double s){
        if(s <= 0){
            System.out.println("Error: S must more than zero!!!");
              return;
        }
        this.s = s;
    }
}
