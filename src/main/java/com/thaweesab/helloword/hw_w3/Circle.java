package com.thaweesab.helloword.hw_w3;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author acer
 */
public class Circle {
       double r;
    public static final double pi = 22.0/7;
    public Circle (double r){
        this.r = r ;
        
    }
    public double calArea(){
        return pi * r *r ;
    }
    public double getR(){
        return r;
    }
      public void setR(double r) {
          if(r<=0){
              System.out.println("Error: Radius must more than zero!!!");
              return;
          }
          this.r = r ;
    }
    public String toString (){
        return "Area of circle(r = "+this.getR()+") is "+this.calArea();
    }
}
